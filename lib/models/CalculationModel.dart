import 'package:alcotester_flutter_app/models/ConsumedDrinkModel.dart';

class CalculationModel {
  double bac;
  List<ConsumedDrinkModel> consumedDrinks;
  DateTime start;
  double alcoholLevel;
  DateTime lastDrinkAddedAt;
  int hoursToSober;
  int minutesToSober;

  CalculationModel({ this.bac, this.consumedDrinks, this.start, this.alcoholLevel, this.lastDrinkAddedAt, this.hoursToSober, this.minutesToSober});
}