import 'package:alcotester_flutter_app/db/structure/ConfigTable.dart';

class ConfigModel {
  int id;
  int gender;
  int metabolism;
  double weight;
  double bacDrivingLimit;
  bool soundsEnabled;
  bool confirmChoices;

  ConfigModel({ this.id, this.gender, this.metabolism, this.weight, this.bacDrivingLimit, this.soundsEnabled, this.confirmChoices});

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      ConfigTable.COLUMN_ID: id,
      ConfigTable.COLUMN_GENDER: gender,
      ConfigTable.COLUMN_METABOLISM: metabolism,
      ConfigTable.COLUMN_WEIGHT: weight,
      ConfigTable.COLUMN_BAC_DRIVING_LIMIT: bacDrivingLimit,
      ConfigTable.COLUMN_SOUNDS_ENABLED: soundsEnabled ? 1 : 0,
      ConfigTable.COLUMN_CONFIRM_CHOICES: confirmChoices ? 1 : 0,
    };

    if (id != null) {
      map[ConfigTable.COLUMN_ID] = id;
    }

    return map;
  }

  ConfigModel.fromMap(Map<String, dynamic> map) {
    id = map[ConfigTable.COLUMN_ID];
    gender = map[ConfigTable.COLUMN_GENDER];
    metabolism = map[ConfigTable.COLUMN_METABOLISM];
    weight = map[ConfigTable.COLUMN_WEIGHT];
    bacDrivingLimit = map[ConfigTable.COLUMN_BAC_DRIVING_LIMIT];
    soundsEnabled = map[ConfigTable.COLUMN_SOUNDS_ENABLED] == 1;
    confirmChoices = map[ConfigTable.COLUMN_CONFIRM_CHOICES] == 1;
  }
}
