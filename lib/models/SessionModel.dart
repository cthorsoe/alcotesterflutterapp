import 'package:alcotester_flutter_app/db/structure/SessionsTable.dart';

class SessionModel {
  int id;
  DateTime start;
  DateTime end;
  DateTime lastDrinkAddedAt;
  double alcoholLevel;
  bool isActive;

  SessionModel({this.id, this.start, this.end, this.alcoholLevel, this.lastDrinkAddedAt, this.isActive});

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      SessionsTable.COLUMN_START: start.millisecondsSinceEpoch,
      SessionsTable.COLUMN_ALCOHOL_LEVEL: alcoholLevel,
      SessionsTable.COLUMN_IS_ACTIVE: isActive ? 1 : 0,
    };

    if (id != null) {
      map[SessionsTable.COLUMN_ID] = id;
    }
    if (end != null) {
      map[SessionsTable.COLUMN_END] = end.millisecondsSinceEpoch;
    }
    if(lastDrinkAddedAt != null){
      map[SessionsTable.COLUMN_LAST_DRINK_ADDED_AT] = lastDrinkAddedAt.millisecondsSinceEpoch;
    }

    return map;
  }

  SessionModel.fromMap(Map<String, dynamic> map) {
    id = map[SessionsTable.COLUMN_ID];
    start = DateTime.fromMillisecondsSinceEpoch(map[SessionsTable.COLUMN_START]);
    end = map[SessionsTable.COLUMN_END] != null ? DateTime.fromMillisecondsSinceEpoch(map[SessionsTable.COLUMN_END]) : null;
    alcoholLevel = map[SessionsTable.COLUMN_ALCOHOL_LEVEL];
    lastDrinkAddedAt = map[SessionsTable.COLUMN_LAST_DRINK_ADDED_AT] != null ? DateTime.fromMillisecondsSinceEpoch(map[SessionsTable.COLUMN_LAST_DRINK_ADDED_AT]) : null;
    isActive = map[SessionsTable.COLUMN_IS_ACTIVE] == 1;
  }
}