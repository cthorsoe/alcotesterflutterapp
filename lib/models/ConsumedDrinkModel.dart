import 'package:alcotester_flutter_app/db/structure/ConsumedDrinksTable.dart';

class ConsumedDrinkModel {
  int id;
  int drinkTypeId;
  int sessionId;
  DateTime consumedAt;

  ConsumedDrinkModel({this.id, this.drinkTypeId, this.sessionId, this.consumedAt});

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      ConsumedDrinksTable.COLUMN_DRINK_TYPE_ID: drinkTypeId,
      ConsumedDrinksTable.COLUMN_SESSION_ID: sessionId,
      ConsumedDrinksTable.COLUMN_CONSUMED_AT: consumedAt.millisecondsSinceEpoch,
    };

    if (id != null) {
      map[ConsumedDrinksTable.COLUMN_ID] = id;
    }

    return map;
  }

  ConsumedDrinkModel.fromMap(Map<String, dynamic> map) {
    id = map[ConsumedDrinksTable.COLUMN_ID];
    drinkTypeId = map[ConsumedDrinksTable.COLUMN_DRINK_TYPE_ID];
    sessionId = map[ConsumedDrinksTable.COLUMN_SESSION_ID];
    consumedAt = DateTime.fromMillisecondsSinceEpoch(map[ConsumedDrinksTable.COLUMN_CONSUMED_AT]);
  }
}
