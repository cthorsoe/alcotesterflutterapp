import 'package:flutter/material.dart';

class ViewModel {
  ViewModel(String _title, Widget _widget) {
    title = _title;
    widget = _widget;
  }
  String title;
  Widget widget;
}