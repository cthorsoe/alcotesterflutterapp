import 'package:alcotester_flutter_app/db/structure/DrinkTypesTable.dart';

class DrinkTypeModel {
  int id;
  String name;
  double alcohol;
  double amount;
  double units;
  double alcoholInGrams;
  int iconCode;

  DrinkTypeModel({this.id, this.name, this.alcohol, this.amount, this.units, this.alcoholInGrams, this.iconCode});

  Map<String, dynamic> toMap() {
    
    var map = <String, dynamic>{
      DrinkTypesTable.COLUMN_NAME: name,
      DrinkTypesTable.COLUMN_ALCOHOL: alcohol,
      DrinkTypesTable.COLUMN_AMOUNT: amount,
      DrinkTypesTable.COLUMN_UNITS: units,
      DrinkTypesTable.COLUMN_ALCOHOL_IN_GRAMS: alcoholInGrams,
      DrinkTypesTable.COLUMN_ICON_CODE: iconCode
    };

    if (id != null) {
      map[DrinkTypesTable.COLUMN_ID] = id;
    }

    return map;
  }

  DrinkTypeModel.fromMap(Map<String, dynamic> map) {
    id = map[DrinkTypesTable.COLUMN_ID];
    name = map[DrinkTypesTable.COLUMN_NAME];
    alcohol = map[DrinkTypesTable.COLUMN_ALCOHOL];
    amount = map[DrinkTypesTable.COLUMN_AMOUNT];
    units = map[DrinkTypesTable.COLUMN_UNITS];
    alcoholInGrams = map[DrinkTypesTable.COLUMN_ALCOHOL_IN_GRAMS];
    iconCode = map[DrinkTypesTable.COLUMN_ICON_CODE];
  }
}
