import 'package:flutter/widgets.dart';

class AlcoTesterIcons {
  AlcoTesterIcons._();

  static const _kFontFam = 'AlcoTesterIcons';
  static const String _kFontPkg = null;

  static const int shotIconCode = 0xe800;
  static const int canIconCode = 0xe801;
  static const int beerIconCode = 0xe802;
  static const int wineIconCode = 0xe803;
  static const int cocktailIconCode = 0xe804;

  static const IconData shot = IconData(shotIconCode, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData can = IconData(canIconCode, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData beer = IconData(beerIconCode, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData wine = IconData(wineIconCode, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData cocktail = IconData(cocktailIconCode, fontFamily: _kFontFam, fontPackage: _kFontPkg);

  static IconData getCustomIcon(int iconCode) {
    return IconData(iconCode, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  }
}
