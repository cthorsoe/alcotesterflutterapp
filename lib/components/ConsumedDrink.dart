import 'package:alcotester_flutter_app/icons/AlcoTesterIcons.dart';
import 'package:alcotester_flutter_app/variables/AppVariables.dart';
import 'package:flutter/material.dart';
import 'package:alcotester_flutter_app/models/DrinkTypeModel.dart';
import 'package:alcotester_flutter_app/helpers/DateFormatter.dart';

enum Answers { CANCEL }

class ConsumedDrink extends StatefulWidget {
  const ConsumedDrink({ 
    Key key, 
    @required this.title, 
    @required this.drinkType, 
    @required this.date,
    @required this.inBACCalculation,
  }) : super(key: key);

  final String title;
  final DrinkTypeModel drinkType;
  final DateTime date;
  final bool inBACCalculation;

  @override
  ConsumedDrinkState createState() => ConsumedDrinkState(title, drinkType, date, inBACCalculation);
}

class ConsumedDrinkState extends State<ConsumedDrink> {
  ConsumedDrinkState(this.title, this.drinkType, this.date, this.inBACCalculation);

  final String title;
  final DrinkTypeModel drinkType;
  final DateTime date;
  final bool inBACCalculation;

  void showDrinkDetails(BuildContext context, String date, bool inCalculation) {
    SimpleDialog dialog = SimpleDialog(
      title: Text('Drink details'),
      children: [        
        SimpleDialogOption(
          child: Row(
            children: [
              Text('Drink type: ', style: TextStyle(fontWeight: FontWeight.w600)),
              Text(drinkType.name)
            ]
          )
        ),
        SimpleDialogOption(
          child: Row(
            children: [
              Text('Alcohol content: ', style: TextStyle(fontWeight: FontWeight.w600)),
              Text('${ drinkType.alcohol }%')
            ]
          )
        ),
        SimpleDialogOption(
          child: Row(
            children: [
              Text('Amount: ', style: TextStyle(fontWeight: FontWeight.w600)),
              Text('${ drinkType.amount } cl')
            ]
          )
        ),
        SimpleDialogOption(
          child: Row(
            children: [
              Text('Consumed at: ', style: TextStyle(fontWeight: FontWeight.w600)),
              Text(date)
            ]
          )
        ), 
        SimpleDialogOption(
          child: Row(
            children: [
              Text('In current calculation: ', style: TextStyle(fontWeight: FontWeight.w600)),
              Text(inCalculation ? "Yes" : "No")
            ]
          )
        ), 
        SimpleDialogOption(
          onPressed: () { Navigator.pop(context); },
          child: Text(
            'Close', 
            textAlign: TextAlign.right,
            style: TextStyle(
              fontWeight: FontWeight.w600,
              color: Colors.blue[500]
            )
          ),
        ),
      ],
    );

    showDialog(
        context: context,
        builder: (BuildContext context) {
          return dialog;
        }
    );
  }

  @override
  Widget build(BuildContext context) {
    final Color textColor = inBACCalculation ? Colors.black87 : Colors.black26;
    
    return Card(
      color: inBACCalculation ? Colors.blue[50] : Colors.white,
      child: Padding(
        padding: EdgeInsets.symmetric(vertical: AppVariables.ConsumedListItemVerticalPadding),
        child: ListTile(
          leading: Padding(
            padding: EdgeInsets.fromLTRB(6, 8, 0, 8),
            child: Icon(AlcoTesterIcons.getCustomIcon(drinkType.iconCode), color: textColor),
          ),
          title: Padding(
            padding: EdgeInsets.symmetric(vertical: 8),
            child: Text(
              title,
              style: TextStyle(
                fontSize: AppVariables.ConsumedListItemHeaderFontSize,
                color: textColor, 
                fontWeight: FontWeight.w600
              ),
            ),
          ),
          subtitle: Padding(
            padding: EdgeInsets.symmetric(vertical: 2),
            child: Text(
              DateFormatter.formatDate(date, true, true),
              style: TextStyle(
                fontSize: AppVariables.AppFontSize,
                color: textColor
              ),
            ),
          ),
          onTap: () { 
            showDrinkDetails(
              context, 
              DateFormatter.formatDate(date), 
              inBACCalculation
            ); 
          },
        ),
      )
      
    );
  }
}