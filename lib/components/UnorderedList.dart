import 'package:alcotester_flutter_app/variables/AppVariables.dart';
import 'package:flutter/material.dart';

class UnorderedList extends StatelessWidget {
  const UnorderedList({ 
    Key key, 
    this.items, 
    this.icon = Icons.fiber_manual_record, 
    this.iconColor = Colors.black87, 
    this.iconSize = 12,
    this.lineSpace = 2,
    this.fontColor = Colors.black87,
    this.fontSize = AppVariables.AppFontSize
  }) : super(key: key);

  final List<String> items;
  final IconData icon;
  final Color iconColor;
  final double iconSize;
  final double lineSpace;
  final Color fontColor;
  final double fontSize;


  @override
  Widget build(BuildContext context) {
    final double containerWidth = MediaQuery.of(context).size.width * 0.8;
    return  Column(
        children: items.map<Widget>(
          (item) => Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
                Padding(
                  padding: EdgeInsets.fromLTRB(0, 4, 0, 0),
                  child: Icon(icon, color: iconColor, size: iconSize)
                ),
                Container(
                  width: containerWidth,
                  padding: EdgeInsets.symmetric(horizontal: 4, vertical: lineSpace),
                  child: Text(item, style: TextStyle(fontSize: fontSize, color: fontColor))
                )
              ]
          )
        ).toList(),    
    );
  }
}