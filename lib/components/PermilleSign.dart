import 'package:flutter/material.dart';

class PermilleSign extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Stack(
      clipBehavior: Clip.none,
      children: [
        Positioned(
          bottom: -7,
          left: 2,
          child: Text(
            "0", 
            style: TextStyle(
              fontSize: 22, 
              fontWeight: FontWeight.w600
            )
          )
        ),
        Positioned(
          left: 9,
          bottom: -22,
          child: Text(
            "/", 
            style: TextStyle(
              fontSize: 38, 
              fontWeight: FontWeight.w600
            )
          )
        ),
        Positioned(
          left: 18,
          bottom: -20,
          child: Text(
            "00", 
            style: TextStyle(
              fontSize: 22, 
              fontWeight: FontWeight.w600
            )
          )
        ),
        Positioned( // ?????
          child: Text(
            ""
          )
        ),
      ],
    );
  }
}