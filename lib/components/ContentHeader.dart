import 'package:alcotester_flutter_app/variables/AppVariables.dart';
import 'package:flutter/material.dart';

class ContentHeader extends StatelessWidget {
  const ContentHeader({Key key, @required this.header, this.fontWeight = FontWeight.w600, this.fontSize = AppVariables.AppRegularHeaderFontSize}) : super(key: key);
  final String header;
  final FontWeight fontWeight;
  final double fontSize;
  @override 
  Widget build(BuildContext context) {
    return Text(
      header, 
      style: TextStyle(
        fontWeight: FontWeight.w600,
        fontSize: fontSize
      )
    );
  }
}