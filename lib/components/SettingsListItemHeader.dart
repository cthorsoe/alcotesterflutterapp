import 'package:alcotester_flutter_app/variables/AppVariables.dart';
import 'package:flutter/material.dart';

class SettingsListItemHeader extends StatelessWidget {
  const SettingsListItemHeader({Key key, @required this.header}) : super(key: key);
  final String header;
  @override 
  Widget build(BuildContext context) {
    return Text(
      header, 
      style: TextStyle(
        fontSize: AppVariables.SettingsListItemFontSize,
        fontWeight: FontWeight.w600
      )
    );
  }
}