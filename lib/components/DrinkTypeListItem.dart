import 'package:alcotester_flutter_app/helpers/DoubleFormatter.dart';
import 'package:alcotester_flutter_app/models/DrinkTypeModel.dart';
import 'package:alcotester_flutter_app/icons/AlcoTesterIcons.dart';
import 'package:alcotester_flutter_app/variables/AppVariables.dart';
import 'package:flutter/material.dart';

class DrinkTypeListItem extends StatefulWidget {
  const DrinkTypeListItem({ 
    Key key, 
    @required this.drinkType, 
  }) : super(key: key);
  final DrinkTypeModel drinkType;

  @override
  DrinkTypeListItemState createState() => DrinkTypeListItemState(drinkType);
}

class DrinkTypeListItemState extends State<DrinkTypeListItem> {
  DrinkTypeListItemState(this.drinkType);

  final DrinkTypeModel drinkType;

  @override
  Widget build(BuildContext context) {
    return Card(
      color: Colors.white,
      child: ListTile(
        leading: Padding(
          padding: EdgeInsets.fromLTRB(6, 26, 0, 0),
          child: Icon(AlcoTesterIcons.getCustomIcon(drinkType.iconCode), color: Colors.black87),
        ),
        title: Padding(
          padding: EdgeInsets.fromLTRB(0, 12, 0, 8),
          child: Text(
            drinkType.name,
            style: TextStyle(
              fontSize: AppVariables.InformationDrinkTypeHeaderFontSize,
              color: Colors.black87, 
              fontWeight: FontWeight.w600
            ),
          )
        ),
        subtitle: Padding(
          padding:EdgeInsets.fromLTRB(0, 0, 0, 8),
          child:Text(
            "Alcohol: ${ drinkType.alcohol.toString() }%\nAmount: ${ drinkType.amount.toString() } cl\nUnits: ${ DoubleFormatter.format(drinkType.units) }\nGrams: ${ drinkType.alcoholInGrams }",
            style: TextStyle(color: Colors.black87, fontSize: AppVariables.InformationDrinkTypeContentFontSize),
          )
        )
      ),
    );
  }
}