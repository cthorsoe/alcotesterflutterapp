import 'package:alcotester_flutter_app/components/ContentHeader.dart';
import 'package:alcotester_flutter_app/variables/AppVariables.dart';
import 'package:flutter/material.dart';
import 'package:package_info/package_info.dart';
import 'package:alcotester_flutter_app/db/DataProvider.dart';
import 'package:alcotester_flutter_app/models/DrinkTypeModel.dart';
import 'package:alcotester_flutter_app/components/DrinkTypeListItem.dart';
import 'package:alcotester_flutter_app/components/UnorderedList.dart';

class Information extends StatefulWidget {
  @override
  InformationState createState() => InformationState();
}

class InformationState extends State<Information> {
  PackageInfo _packageInfo = PackageInfo(
    appName: '...',
    packageName: '...',
    version: '...',
    buildNumber: '...',
  );
  List<DrinkTypeModel> drinkTypes;

  @override
  void initState() {
    super.initState();
    _initPackageInfo();
    DataProvider.data.getDrinkTypes().then((drinkTypeList) => 
      setState(() {
        drinkTypes = drinkTypeList;
      })
    );
  }

  Future<void> _initPackageInfo() async {
    final PackageInfo info = await PackageInfo.fromPlatform();
    setState(() {
      _packageInfo = info;
    });
  }

  Widget drinkTypesList(){
    if(drinkTypes == null){
      return Container();
    }else {
      List<Widget> drinkTypeWidgets = [];
      for(var i = 0; i < drinkTypes.length; i++){
          var drinkType = drinkTypes[i];
          drinkTypeWidgets.add(DrinkTypeListItem(drinkType: drinkType));
      }
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
        Padding(
            padding: EdgeInsets.symmetric(vertical: AppVariables.InformationParagraphVericalPadding),
            child: ContentHeader(header: "Drink types")
        ),
        ListView(
          padding: EdgeInsets.symmetric(vertical: AppVariables.InformationParagraphVericalPadding),
          physics: ClampingScrollPhysics(), 
          shrinkWrap: true,
          children: drinkTypeWidgets,
        )
      ]);
    }
  }

  Widget build(BuildContext context) {    
    return Scaffold(
      appBar: AppBar(
        title: Text('Information', style: TextStyle(
            color: Colors.white,
            fontSize: AppVariables.AppBarFontSize
          )
        ),
        centerTitle: true,
        iconTheme: IconThemeData(
          color: Colors.white,
        ),
      ),
      body:  ListView(
        padding: EdgeInsets.symmetric(horizontal: 16),
        children: [
          Padding(
            padding: EdgeInsets.fromLTRB(0, 8, 0, 0),
            child: 
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Text("AlcoTester", style: TextStyle(fontSize: AppVariables.InformationHeaderFontSize, fontWeight: FontWeight.bold) ),
                  Padding(padding: EdgeInsets.fromLTRB(4, 0, 0, 4),
                    child: Text("v" + _packageInfo.version, style: TextStyle(fontSize: 10))
                  ),
                ],
              ),
          ),
          Padding(
            padding: EdgeInsets.symmetric(vertical: 8),
            child: Card(
              color: Colors.red[400],
              child: 
                Padding(
                padding: EdgeInsets.all(16),
                child: Text(
                  "WARNING!\n The calculation is only an indication and CANNOT IN ANY WAY LEGALIZE DRIVING A CAR", 
                  style: TextStyle(
                    fontSize: AppVariables.InformationAlertFontSize, 
                    fontWeight: FontWeight.w600
                  ), 
                  textAlign: TextAlign.center
                )
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.symmetric(vertical: AppVariables.InformationParagraphVericalPadding),
            child: ContentHeader(header: "About")
          ),
          Padding(
            padding: EdgeInsets.symmetric(vertical: AppVariables.InformationParagraphVericalPadding),
            child: Text(
              "AlcoTester is to be used simultaneously with units consumed. It is assumed that the units are consumed faster than the alcohol metabolism so the calculation can be accurate.",
              style: TextStyle(fontSize: AppVariables.AppFontSize),
            ),
          ),
          Padding(
            padding: EdgeInsets.symmetric(vertical: AppVariables.InformationParagraphVericalPadding),
            child: Text("Before AlcoTester is taken into use, the gender, alcohol metabolism and weight are to be specified under settings. Here you also specify the road traffic alcohol limit for the country you are located in.",
              style: TextStyle(fontSize: AppVariables.AppFontSize),
            ),
          ),
          Padding(
            padding: EdgeInsets.symmetric(vertical: AppVariables.InformationParagraphVericalPadding),
            child: Text("Each time a new unit is started the proper type is selected in the B.A. Calculator, and calculation is made continuously",
              style: TextStyle(fontSize: AppVariables.AppFontSize),
            ),
          ),
          Padding(
            padding: EdgeInsets.symmetric(vertical: AppVariables.InformationParagraphVericalPadding),
            child: Text("To clear entered units, press the trash icon.",
              style: TextStyle(fontSize: AppVariables.AppFontSize),
            ),
          ),
          Padding(
            padding: EdgeInsets.symmetric(vertical: AppVariables.InformationParagraphVericalPadding),
            child: drinkTypesList(),
          ),
          Padding(
            padding: EdgeInsets.symmetric(vertical: AppVariables.InformationParagraphVericalPadding),
            child: ContentHeader(header: "Details")
          ),
          Padding(
            padding: EdgeInsets.symmetric(vertical: AppVariables.InformationParagraphVericalPadding),
            child: UnorderedList(
              items: [
                "Version: " + _packageInfo.version, 
                "Developed by: Michael Thorsø & Christian Thorsø", 
                "Email: alcotester@thorsoe.dk"
              ]
            ),
          ),
          Padding(
            padding: EdgeInsets.symmetric(vertical: 8),
            child: Image.asset('assets/images/thorsoe.png')
          )
        ],
      ),
    );
  }
}