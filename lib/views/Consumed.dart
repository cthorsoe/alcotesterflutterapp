import 'package:alcotester_flutter_app/variables/AppVariables.dart';
import 'package:flutter/material.dart';
import 'package:alcotester_flutter_app/db/DataProvider.dart';
import 'package:alcotester_flutter_app/models/ConsumedDrinkModel.dart';
import 'package:alcotester_flutter_app/models/DrinkTypeModel.dart';
import 'package:alcotester_flutter_app/components/ConsumedDrink.dart';
import 'package:provider/provider.dart';

class Consumed extends StatefulWidget {
  const Consumed({Key key}) : super(key: key);

  @override
  ConsumedState createState() => ConsumedState();
}

class ConsumedState extends State<Consumed> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(child: Consumer<DataProvider>(
        builder: (context, data, child) {
            double currentAlcoholLevel = data.calculation != null ? data.calculation.alcoholLevel : 0;
            double loopedAlcoholLevel = 0;
            bool checkIfInCalculation = currentAlcoholLevel > 0;
            if(data.drinkTypesList == null || data.consumedDrinksList == null){
              return Center(
                child: Text("Loading consumed drinks...", style: TextStyle(fontSize: AppVariables.AppFontSize))
              );
            } else if(data.consumedDrinksList.length == 0){
              return Center(
                child: Text("No consumed drinks found.", style: TextStyle(fontSize: AppVariables.AppFontSize))
              );
            } else {
              return ListView.builder(
                padding: EdgeInsets.fromLTRB(AppVariables.ConsumedListViewPadding, AppVariables.ConsumedListViewPadding, AppVariables.ConsumedListViewPadding, 0),
                itemBuilder: (BuildContext context, int index) {
                    ConsumedDrinkModel consumedDrink = data.consumedDrinksList[index];
                    if(consumedDrink != null && data.drinkTypesList != null){
                      DrinkTypeModel drinkType = data.drinkTypesList.firstWhere((x) => x.id == consumedDrink.drinkTypeId);
                      bool inCalculation = false;
                      if(checkIfInCalculation){
                        bool inCurrentSession = data.currentSession != null && data.currentSession.id == consumedDrink.sessionId;
                        inCalculation = inCurrentSession && currentAlcoholLevel >= loopedAlcoholLevel;
                        loopedAlcoholLevel += drinkType.alcoholInGrams;
                        if(!inCalculation) checkIfInCalculation = false;
                      }
                      
                      return ConsumedDrink(title: drinkType.name, drinkType: drinkType, date: consumedDrink.consumedAt, inBACCalculation: inCalculation);
                    }
                    else {
                      return Container();
                    }
                },
                itemCount: data.consumedDrinksList.length,
              );
            }
            
          }
        ),
      )
    );
  }
}
