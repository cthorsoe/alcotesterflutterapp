import 'package:alcotester_flutter_app/components/SettingsListItemHeader.dart';
import 'package:alcotester_flutter_app/variables/AppVariables.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:alcotester_flutter_app/db/DataProvider.dart';

class Settings extends StatefulWidget {

  const Settings({Key key}) : super(key: key);
  
  @override
  SettingsState createState() => SettingsState();
}

class SettingsState extends State<Settings> {

  void _genderChanged(int newGender) {
    DataProvider.data.updateConfig(gender: newGender);
  }

  void _metabolismChanged(int newMetabolism) {
    DataProvider.data.updateConfig(metabolism: newMetabolism);
  }

  void _weightChanged(String newWeight) {
    double weight = newWeight.isNotEmpty ? double.parse(newWeight) : 0;
    DataProvider.data.updateConfig(weight: weight);
  }

  void _bacLimitChanged(String newBacLimit) {
    double bacLimit = newBacLimit.isNotEmpty ? double.parse(newBacLimit) : 0;
    DataProvider.data.updateConfig(bacLimit: bacLimit);
  }
  
  void _soundsEnabledChanged(bool soundsEnabled) {
    print("UPDATE SOUNDS ENABLED: " + (soundsEnabled ? "YES" : "NO"));
    DataProvider.data.updateConfig(soundsEnabled: soundsEnabled);
  }
  
  void _confirmChoicesChanged(bool confirmChoices) {
    print("UPDATE CONFIRM CHOICES: " + (confirmChoices ? "YES" : "NO"));
    DataProvider.data.updateConfig(confirmChoices: confirmChoices);
  }
  
  Widget build(BuildContext context) {
    List<int> genders = [0, 1];

    List<int> metabolisms = [0, 1, 2];

    return Consumer<DataProvider>(
      builder: (context, data, child) {
        if(data.config == null) {
          return Center(
            child: Text("Loading settings...", style: TextStyle(fontSize: AppVariables.AppFontSize))
          );
        }
        else {
          return ListView(children: [
            Container(
              padding: EdgeInsets.symmetric(
                horizontal: AppVariables.SettingsListItemHorizontalPadding, 
                vertical: AppVariables.SettingsListItemVerticalPadding
              ),
              child: Row(
                children: [
                  Expanded(
                    flex: 1,
                    child: SettingsListItemHeader(header: "Gender")
                  ),
                    Container(
                    width: 100,
                    child: DropdownButton<int>(
                      value: data.config.gender,
                      icon: const Icon(Icons.arrow_downward),
                      iconSize: AppVariables.SettingsDropDownIconSize,
                      elevation: AppVariables.SettingsDropDownElevation,
                      style: const TextStyle(
                        color: Colors.blue,
                        fontSize: AppVariables.SettingsListItemFontSize
                      ),
                      underline: Container(
                        height: 2,
                        color: Colors.blue[400],
                      ),
                      onChanged: (int newGender) {
                        _genderChanged(newGender);
                      },
                      items: genders
                          .map<DropdownMenuItem<int>>((int value) {
                        return DropdownMenuItem<int>(
                          value: value,
                          child: Text(data.genders[value].name)
                        );
                      }).toList(),
                    )
                  )
                ]
              )
            ),
            Container(
              padding: EdgeInsets.symmetric(
                horizontal: AppVariables.SettingsListItemHorizontalPadding, 
                vertical: AppVariables.SettingsListItemVerticalPadding
              ),
              color: Colors.blue[50],
              child: Row(
                children: [
                  Expanded(
                    flex: 1,
                    child: SettingsListItemHeader(header: "Metabolism")
                  ),
                  Container(
                    width: 100,
                    child: DropdownButton<int>(
                      value: data.config.metabolism,
                      icon: const Icon(Icons.arrow_downward),
                      iconSize: AppVariables.SettingsDropDownIconSize,
                      elevation: AppVariables.SettingsDropDownElevation,
                      style: const TextStyle(
                        color: Colors.blue,
                        fontSize: AppVariables.SettingsListItemFontSize
                      ),
                      underline: Container(
                        height: 2,
                        color: Colors.blue[400],
                      ),
                      onChanged: (int newMetabolism) {
                        _metabolismChanged(newMetabolism);
                      },
                      items: metabolisms
                          .map<DropdownMenuItem<int>>((int value) {
                        return DropdownMenuItem<int>(
                          value: value,
                          child: Text(data.metabolisms[value].name)
                        );
                      }).toList(),
                    )
                  )
                ]
              )
            ),
            Container(
              padding: EdgeInsets.symmetric(
                horizontal: AppVariables.SettingsListItemHorizontalPadding, 
                vertical: AppVariables.SettingsListItemVerticalPadding
              ),
              child: Row(
                children: [
                  Expanded(
                    flex: 1,
                    child: SettingsListItemHeader(header: "Weight")
                  ),
                  Container(
                    width: 100,                
                    child: TextFormField(
                      initialValue: data.config.weight.toString(),
                      style: TextStyle(
                        fontSize:AppVariables.SettingsListItemFontSize
                      ),
                      onChanged: (String newWeight) {
                        _weightChanged(newWeight);
                      },
                      autofocus: false,
                      textAlign: TextAlign.end,
                      keyboardType: TextInputType.numberWithOptions(decimal:true),
                      inputFormatters: [
                          FilteringTextInputFormatter.allow(RegExp(r'^\d+\.?\d{0,1}')),
                      ]
                    )
                  )
                ]
              )
            ),
            Container(
              padding: EdgeInsets.symmetric(
                horizontal: AppVariables.SettingsListItemHorizontalPadding, 
                vertical: AppVariables.SettingsListItemVerticalPadding
              ),
              color: Colors.blue[50],
              child: Row(
                children: [
                  Expanded(
                    flex: 1,
                    child: SettingsListItemHeader(header: "Road traffic alcohol limit")
                  ),
                  Container(
                    width: 100,                
                    child: TextFormField(
                      initialValue: data.config.bacDrivingLimit.toString(),
                      onChanged: (String newBacLimit) {
                        _bacLimitChanged(newBacLimit);
                      },
                      style: TextStyle(
                        fontSize:AppVariables.SettingsListItemFontSize
                      ),
                      autofocus: false,
                      textAlign: TextAlign.end,
                      keyboardType: TextInputType.numberWithOptions(decimal:true),
                      inputFormatters: [
                          FilteringTextInputFormatter.allow(RegExp(r'^\d+\.?\d{0,2}')),
                      ]
                    )
                  )
                ]
              )
            ),
            Container(
              padding: EdgeInsets.symmetric(
                horizontal: AppVariables.SettingsListItemHorizontalPadding, 
                vertical: AppVariables.SettingsListItemVerticalPadding
              ),
              child: Row(
                children: [
                  Expanded(
                    flex: 1,
                    child: SettingsListItemHeader(header: "Play sounds for button press")
                  ),
                  Container(
                    width: 70,                
                    child: Switch(
                      value: data.config.soundsEnabled,
                      onChanged: (bool soundsEnabled) {
                        _soundsEnabledChanged(soundsEnabled);
                      },
                    )
                  )
                ]
              )
            ),
            Container(
              padding: EdgeInsets.symmetric(
                horizontal: AppVariables.SettingsListItemHorizontalPadding, 
                vertical: AppVariables.SettingsListItemVerticalPadding
              ),
              color: Colors.blue[50],
              child: Row(
                children: [
                  Expanded(
                    flex: 1,
                    child: SettingsListItemHeader(header: "Confirm choices")
                  ),
                  Container(
                    width: 70,                
                    child: Switch(
                      value: data.config.confirmChoices,
                      onChanged: (bool confirmChoices) {
                        _confirmChoicesChanged(confirmChoices);
                      },
                    )
                  )
                ]
              )
            ),
          ]);
        }
      }
    );
  }
}