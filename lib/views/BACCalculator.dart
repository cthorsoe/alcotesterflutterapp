import 'package:alcotester_flutter_app/db/DataProvider.dart';
import 'package:alcotester_flutter_app/helpers/DateFormatter.dart';
import 'package:alcotester_flutter_app/icons/AlcoTesterIcons.dart';
import 'package:alcotester_flutter_app/variables/AppVariables.dart';
import 'package:flutter/material.dart';
import 'package:alcotester_flutter_app/models/ConsumedDrinkModel.dart';
import 'package:alcotester_flutter_app/models/DrinkTypeModel.dart';
import 'package:alcotester_flutter_app/components/PermilleSign.dart';
import 'package:provider/provider.dart';

class BACCalculator extends StatefulWidget {

  const BACCalculator({Key key}) : super(key: key);

  @override
  BACCalculatorState createState() => BACCalculatorState();
}

class BACCalculatorState extends State<BACCalculator>{

  @override
  void initState() {
    super.initState();
    
  }

  @override
  void dispose() {
    super.dispose();
  }

  void addConsumedDrink(DrinkTypeModel drinkType){
    /* if(DataProvider.data.calculation != null && DataProvider.data.calculation.bac <= 0){
      if(DataProvider.data.config.confirmChoices){
        Navigator.of(context).pop();
      }
      confirmResetBeforeAdding(drinkType);
    }else {
      ConsumedDrinkModel consumedDrink = ConsumedDrinkModel(drinkTypeId: drinkType.id, consumedAt: DateTime.now());
      DataProvider.data.insertConsumedDrink(consumedDrink, true);
    } */
    ConsumedDrinkModel consumedDrink = ConsumedDrinkModel(drinkTypeId: drinkType.id, consumedAt: DateTime.now());
    DataProvider.data.insertConsumedDrink(consumedDrink, true);
  }

  Future resetCalculation() async {
    await DataProvider.data.clearCalculation();
  }

  void calculateBAC() {
     DataProvider.data.calculate();
  }

  void confirmResetBeforeAdding(DrinkTypeModel drinkType){
    AlertDialog dialog = AlertDialog(
      title: Text('Reset first?'),
      content: Text("Do you wish to reset the session before adding this ${ drinkType.name }?"),
      actions: <Widget>[
        TextButton(
          child: Text('Yes'),
          onPressed: () async {
            await resetCalculation();
            ConsumedDrinkModel consumedDrink = ConsumedDrinkModel(drinkTypeId: drinkType.id, consumedAt: DateTime.now());
            DataProvider.data.insertConsumedDrink(consumedDrink, true);
            Navigator.of(context).pop();
          },
        ),
        TextButton(
          child: Text('No'),
          onPressed: () {
            ConsumedDrinkModel consumedDrink = ConsumedDrinkModel(drinkTypeId: drinkType.id, consumedAt: DateTime.now());
            DataProvider.data.insertConsumedDrink(consumedDrink, true);
            Navigator.of(context).pop();
          },
        ),
      ],
    );

    showDialog(
        context: context,
        builder: (BuildContext context) {
          return dialog;
        }
    );
  }

  String getTimeToSoberString(int hoursToSober, int minutesToSober){
    return "Sober in ${ ( hoursToSober > 0 ? hoursToSober.toString() + (hoursToSober > 1 ? " hours" : " hour") + " and" : "" ) } ${ minutesToSober.toString() + (minutesToSober > 1 ? " minutes" : " minute") }.";
  }

  String getDrivingInstructionString(){
    var calc = DataProvider.data.calculation;
    var conf = DataProvider.data.config;
    var questionableLimit = conf != null ? (conf.bacDrivingLimit - 0.3) : 0.2;
    if(questionableLimit <= 0){
      questionableLimit = 0;
    }
    if(calc == null || calc.bac == 0){
      return "Should be safe to drive.";
    }
    if(calc.bac >= questionableLimit && calc.bac < (conf != null ? conf.bacDrivingLimit : 0.5)){
      return "Your BAC should be lower than the limit, but still not advisable to drive.";
    }
    return "Your BAC is way too high to be driving at the moment.";
  }

  void confirmAddConsumedDrink(DrinkTypeModel drinkType){
    AlertDialog dialog = AlertDialog(
      title: Text('Add ${ drinkType.name }?'),
      content: Text("Do you wish to add a ${ drinkType.name }?"),
      actions: <Widget>[
        TextButton(
          child: Text('Add'),
          onPressed: () {
            if(DataProvider.data.calculation != null && DataProvider.data.calculation.bac <= 0){
              Navigator.of(context).pop();
              confirmResetBeforeAdding(drinkType);
            }else {
              addConsumedDrink(drinkType);
              Navigator.of(context).pop();
            }
          },
        ),
        TextButton(
          child: Text('Cancel'),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
      ],
    );

    showDialog(
        context: context,
        builder: (BuildContext context) {
          return dialog;
        }
    );
  }

  void confirmClearCalculation(){
    AlertDialog dialog = AlertDialog(
      title: Text('Reset calculation?'),
      content: Text("Do you wish to reset the calculation?"),
      actions: <Widget>[
        TextButton(
          child: Text('Reset'),
          onPressed: () {
            resetCalculation();
            Navigator.of(context).pop();
          },
        ),
        TextButton(
          child: Text('Cancel'),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
      ],
    );

    showDialog(
        context: context,
        builder: (BuildContext context) {
          return dialog;
        }
    );
  }

  Widget clearCalculationButton(bool show, bool confirm){
    if(show){
      return Card(
        color: Colors.blue[50],
        shape: CircleBorder(),
        child: IconButton(
            tooltip: "Clear Session",
            onPressed: () { 
              confirm ? confirmClearCalculation() : resetCalculation(); 
            },
            icon: Icon(Icons.delete),
          ),
      );
    } else {
      return null;
    }
  }

  @override
  Widget build(BuildContext context) {   

    return Container(
      child: Consumer<DataProvider>(
        builder: (context, data, child) {
          if (data.config == null || data.consumedDrinksList == null || data.drinkTypesList == null) {
            return Center(
              child: Text("Loading calculation...", style: TextStyle(fontSize: AppVariables.AppFontSize))
            );
          } else {
            return ListView(
              padding:EdgeInsets.fromLTRB(8, 8, 8, 0),
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Expanded(
                      flex: 1,
                      child: Center(
                        /*child:Text(
                          data.calculation == null || (data.config.bacDrivingLimit - 0.3) >= data.calculation.bac ? "Safe" : 
                          data.calculation.bac >= data.config.bacDrivingLimit ? "Unsafe" : 
                          "Questionable")*/
                      )
                    ),
                    Expanded(
                      flex: 0,
                      child: Center(
                        child: Text((data.calculation != null ? data.calculation.bac : 0).toStringAsFixed(2), style: TextStyle(fontSize: AppVariables.CalculatorBACFontSize, fontWeight: FontWeight.bold))
                      )
                    ),
                    Expanded(
                      flex: 1,
                      child: PermilleSign()
                    )
                  ],
                ),
                Container(
                  child: Text(
                    getDrivingInstructionString(),
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: AppVariables.AppFontSize,
                      fontStyle: FontStyle.italic
                    ),
                  ),
                ),
                Container(
                  child: Row(
                    children: data.drinkTypesList.map<Widget>((drinkType) => 
                      Expanded(
                        child: Card(
                          shape: CircleBorder(),
                          color: Colors.blue[50],
                          child: InkWell(
                            splashColor: Colors.blue.withAlpha(30),
                            onTap: () {
                              data.config.confirmChoices ? confirmAddConsumedDrink(drinkType) : data.calculation != null && data.calculation.bac <= 0 ? confirmResetBeforeAdding(drinkType) : addConsumedDrink(drinkType);
                            },
                            child: Padding(
                              padding: EdgeInsets.symmetric(vertical: 16),
                              child: Column(
                                children: [
                                  Padding(
                                    padding: EdgeInsets.fromLTRB(0, 0, 0, 4),
                                    child: Text(
                                    data.calculation == null ? "0" : data.calculation.consumedDrinks.where((x) => x.drinkTypeId == drinkType.id).length.toString(), 
                                    style: TextStyle(fontSize: 18)),
                                  ),                                  
                                  Icon(AlcoTesterIcons.getCustomIcon(drinkType.iconCode)),
                                ],
                            )
                            )
                          ),
                        )
                      )
                    ).toList()
                  ),
                ),
                Text(
                  data.calculation != null ? "Calculation started ${ DateFormatter.formatDate(data.calculation.start, true, true, true) }." : "",
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: AppVariables.AppFontSize)
                ),
                Text(
                  data.calculation != null && data.calculation.bac >= 0 ? getTimeToSoberString(data.calculation.hoursToSober, data.calculation.minutesToSober) : "",
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: AppVariables.AppFontSize)
                ),
                Center(
                  child: clearCalculationButton(data.calculation != null, data.config.confirmChoices)
                ),
                /*Text(data.genders[data.config.gender].name),
                Text(data.metabolisms[data.config.metabolism].name),
                Text(data.config.weight.toString()),
                Text(data.config.bacDrivingLimit.toString()),
                Text(data.config.soundsEnabled ? "Sounds Enabled" : "Sounds Disabled"),
                Text(data.config.confirmChoices ? "Confirm Choices Enabled" : "Confirm Choices Disabled"),*/
              ],
            );
          }
        }
      )
    );
  }
}