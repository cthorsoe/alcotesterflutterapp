import 'package:flutter/material.dart';
import 'package:alcotester_flutter_app/AlcoTester.dart';
import 'package:alcotester_flutter_app/db/DataProvider.dart';
import 'package:provider/provider.dart';

void main() => runApp(App());

class App extends StatelessWidget {

  @override 
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData( primaryColor: Colors.blue[400] ),
      home: ChangeNotifierProvider(
        create: (_) => DataProvider.data,
        child: AlcoTester()
      )
    );
  }
}