class ConfigTable {
  static const String TABLE_NAME = "config";  
  static const String COLUMN_ID = "id";
  static const String COLUMN_GENDER = "gender";
  static const String COLUMN_METABOLISM = "metabolism";
  static const String COLUMN_WEIGHT = "weight";
  static const String COLUMN_BAC_DRIVING_LIMIT = "bac_driving_limit";
  static const String COLUMN_SOUNDS_ENABLED = "sounds_enabled";
  static const String COLUMN_CONFIRM_CHOICES = "confirm_choices";
}