class ConsumedDrinksTable {
  static const String TABLE_NAME = "consumeddrinks";  
  static const String COLUMN_ID = "id";
  static const String COLUMN_DRINK_TYPE_ID = "drink_type_id";
  static const String COLUMN_SESSION_ID = "session_id";
  static const String COLUMN_CONSUMED_AT = "consumed_at";
}