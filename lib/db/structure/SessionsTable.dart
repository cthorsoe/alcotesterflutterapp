class SessionsTable {
  static const String TABLE_NAME = "sessions";  
  static const String COLUMN_ID = "id";
  static const String COLUMN_START = "start";
  static const String COLUMN_END = "end";
  static const String COLUMN_ALCOHOL_LEVEL = "last_alcohol_level";
  static const String COLUMN_LAST_DRINK_ADDED_AT = "last_drink_added_at";
  static const String COLUMN_IS_ACTIVE = "is_active";
}