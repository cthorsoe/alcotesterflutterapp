class DrinkTypesTable {
  static const String TABLE_NAME = "drinktypes";  
  static const String COLUMN_ID = "id";
  static const String COLUMN_NAME = "name";
  static const String COLUMN_ALCOHOL = "alcohol";
  static const String COLUMN_AMOUNT = "amount";
  static const String COLUMN_UNITS = "units";
  static const String COLUMN_ALCOHOL_IN_GRAMS = "alcohol_in_grams";
  static const String COLUMN_ICON_CODE = "icon_code";
}