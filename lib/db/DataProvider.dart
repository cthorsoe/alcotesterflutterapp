import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:sqflite/sqlite_api.dart'; 
import 'package:alcotester_flutter_app/db/structure/SessionsTable.dart';
import 'package:alcotester_flutter_app/icons/AlcoTesterIcons.dart';
import 'package:alcotester_flutter_app/models/GenderModel.dart';
import 'package:alcotester_flutter_app/models/MetabolismModel.dart';
import 'package:alcotester_flutter_app/models/CalculationModel.dart';
import 'package:alcotester_flutter_app/models/ConfigModel.dart';
import 'package:alcotester_flutter_app/models/ConsumedDrinkModel.dart';
import 'package:alcotester_flutter_app/models/DrinkTypeModel.dart';
import 'package:alcotester_flutter_app/models/SessionModel.dart';
import 'package:alcotester_flutter_app/db/structure/DrinkTypesTable.dart';
import 'package:alcotester_flutter_app/db/structure/ConsumedDrinksTable.dart';
import 'package:alcotester_flutter_app/db/structure/ConfigTable.dart';


class DataProvider extends ChangeNotifier{
  DataProvider._();
  static final DataProvider data = DataProvider._();
  final List<GenderModel> genders = [
    GenderModel(name: "Male", reductionRate: 0.68),
    GenderModel(name: "Female", reductionRate: 0.55),
  ];
  final List<MetabolismModel> metabolisms = [
    MetabolismModel(name: "Low", tolerance: 1),
    MetabolismModel(name: "Medium", tolerance: 1.25),
    MetabolismModel(name: "High", tolerance: 1.5),
  ];
  Database _database;
  List<DrinkTypeModel> drinkTypesList;
  List<ConsumedDrinkModel> consumedDrinksList;
  ConfigModel config;
  SessionModel currentSession;
  CalculationModel calculation;
  Timer calculationTimer;
  final int timerDelay = 60;

  Future<Database> get database async {
    if (_database != null) {
      return _database;
    }

    _database = await createDatabase();

    return _database;
  }

  Future<Database> createDatabase() async {
    String dbPath = await getDatabasesPath();

    return await openDatabase(
      join(dbPath, 'alcotesterDb.db'),
      version: 21,
      onCreate: (Database database, int version) async {
        await createDatabaseTables(database);
      },
      onUpgrade: (Database database, int oldVersion, int newVersion) async {
        await dropDatabaseTables(database);
        await createDatabaseTables(database);
      },
    );
  }

  Future dropDatabaseTables(Database database) async {
    await database.execute("DROP TABLE IF EXISTS ${ DrinkTypesTable.TABLE_NAME }");
    await database.execute("DROP TABLE IF EXISTS ${ ConsumedDrinksTable.TABLE_NAME }");
    await database.execute("DROP TABLE IF EXISTS ${ ConfigTable.TABLE_NAME }");
    await database.execute("DROP TABLE IF EXISTS ${ SessionsTable.TABLE_NAME }");
    return true;
  }

  Future createDatabaseTables(Database database) async {
    await database.execute(
      "CREATE TABLE ${ DrinkTypesTable.TABLE_NAME } ("
      "${ DrinkTypesTable.COLUMN_ID } INTEGER PRIMARY KEY,"
      "${ DrinkTypesTable.COLUMN_NAME } TEXT,"
      "${ DrinkTypesTable.COLUMN_ALCOHOL } REAL,"
      "${ DrinkTypesTable.COLUMN_AMOUNT } REAL,"
      "${ DrinkTypesTable.COLUMN_UNITS } REAL,"
      "${ DrinkTypesTable.COLUMN_ALCOHOL_IN_GRAMS } REAL,"
      "${ DrinkTypesTable.COLUMN_ICON_CODE } INT"
      ")",
    );

    await database.execute(
      "CREATE TABLE ${ SessionsTable.TABLE_NAME } ("
      "${ SessionsTable.COLUMN_ID } INTEGER PRIMARY KEY,"
      "${ SessionsTable.COLUMN_START } INTEGER,"
      "${ SessionsTable.COLUMN_END } INTEGER,"
      "${ SessionsTable.COLUMN_LAST_DRINK_ADDED_AT } INTEGER,"
      "${ SessionsTable.COLUMN_ALCOHOL_LEVEL } REAL,"
      "${ SessionsTable.COLUMN_IS_ACTIVE } INTEGER"
      ")",
    );

    await database.execute(
      "CREATE TABLE ${ ConsumedDrinksTable.TABLE_NAME } ("
      "${ ConsumedDrinksTable.COLUMN_ID } INTEGER PRIMARY KEY,"
      "${ ConsumedDrinksTable.COLUMN_DRINK_TYPE_ID } INTEGER,"
      "${ ConsumedDrinksTable.COLUMN_SESSION_ID } INTEGER,"
      "${ ConsumedDrinksTable.COLUMN_CONSUMED_AT } INTEGER"
      ")",
    );

    await database.execute(
      "CREATE TABLE ${ ConfigTable.TABLE_NAME } ("
      "${ ConfigTable.COLUMN_ID } INTEGER PRIMARY KEY,"
      "${ ConfigTable.COLUMN_GENDER } INTEGER,"
      "${ ConfigTable.COLUMN_METABOLISM } INTEGER,"
      "${ ConfigTable.COLUMN_WEIGHT } REAL,"
      "${ ConfigTable.COLUMN_BAC_DRIVING_LIMIT } REAL,"
      "${ ConfigTable.COLUMN_SOUNDS_ENABLED } INTEGER,"
      "${ ConfigTable.COLUMN_CONFIRM_CHOICES } INTEGER"
      ")",
    );
    

    await database.insert(DrinkTypesTable.TABLE_NAME, 
      DrinkTypeModel(name: "Beer/Cider", alcohol: 4.6, amount: 33, units: 1, alcoholInGrams: 12, iconCode: AlcoTesterIcons.canIconCode).toMap()
    );
    
    await database.insert(DrinkTypesTable.TABLE_NAME, 
      DrinkTypeModel(name: "Draft beer", alcohol: 4.6, amount: 50, units: 1.5, alcoholInGrams: 18, iconCode: AlcoTesterIcons.beerIconCode).toMap()
    );

    await database.insert(DrinkTypesTable.TABLE_NAME, 
      DrinkTypeModel(name: "Shot", alcohol: 40, amount: 2, units: .5, alcoholInGrams: 6, iconCode: AlcoTesterIcons.shotIconCode).toMap()
    );

    await database.insert(DrinkTypesTable.TABLE_NAME, 
      DrinkTypeModel(name: "Cocktail", alcohol: 49, amount: 4, units: 1, alcoholInGrams: 12, iconCode: AlcoTesterIcons.cocktailIconCode).toMap()
    );

    await database.insert(DrinkTypesTable.TABLE_NAME, 
      DrinkTypeModel(name: "Wine", alcohol: 14, amount: 12, units: 1, alcoholInGrams: 12, iconCode: AlcoTesterIcons.wineIconCode).toMap()
    );
        
    await database.insert(ConfigTable.TABLE_NAME, 
      ConfigModel(
        gender: 0, 
        metabolism: 1, 
        weight: 80, 
        bacDrivingLimit: .5, 
        soundsEnabled: true, 
        confirmChoices: true
      ).toMap()
    );

    return;
  }

  /* CALCULATION START */

  void setupCalculationCycle([bool precalculate = true]) {
    if(precalculate){
      data.calculate();
    }
    if(calculationTimer == null || !calculationTimer.isActive){
      calculationTimer = Timer.periodic(Duration(seconds: timerDelay), (Timer t) => data.calculate());
    }
  }

  Future calculate([ConsumedDrinkModel consumedDrink]) async{
    var now = DateTime.now();
    print("CALLED CALCULATION ${ now.toString() }");
    bool createdCalculation = false;

    //setup for calculation
    if(calculation == null){
        await setExistingSession();
        if(currentSession != null){
          var consumedInSession = await getConsumedDrinksInSession(currentSession.id);
          if(consumedInSession.length > 0){
            await createCalculation(consumedInSession: consumedInSession, start: currentSession.start);
            createdCalculation = true;
          }
        }
    }
    if (consumedDrink != null) {
      await setConsumedDrinks();
      if(currentSession == null){
        await createSession(now: now);
      }
      if(calculation == null){
        createCalculation(start: currentSession.start);
      }
      if(!createdCalculation){
        calculation.consumedDrinks.insert(0, consumedDrink);
      }
    }

    //calculation
    if(calculation != null){
      var consumedDrinkType = consumedDrink != null && !createdCalculation ? drinkTypesList.firstWhere((x) => x.id == consumedDrink.drinkTypeId) : null;
      //calculation.bac = (100 * (calculateBAC(consumedDrinkType != null && !createdCalculation ? consumedDrinkType.alcoholInGrams : 0) + 0.004999999999999999))/100;
      calculation.bac = calculateBAC(consumedDrinkType != null && !createdCalculation ? consumedDrinkType.alcoholInGrams : 0);
      if(calculation.bac < 0) calculation.bac = 0;
      notifyListeners();  
    }
  }

  double calculateBAC(double consumedDrinkAlcoholInGrams ) {
    final double currentAlcoholLevel = getCurrentAlcoholLevel();
    final double alcoholLevel = currentAlcoholLevel + consumedDrinkAlcoholInGrams;
    final double weight = config.weight;
    final double reductionRate = genders[config.gender].reductionRate;
    final double bac = (alcoholLevel / (weight * reductionRate));
    data.calculation.alcoholLevel = alcoholLevel;
    final double totalMinutesToSober = getMinutesToSober();
    data.calculation.hoursToSober = totalMinutesToSober ~/ 60;
    data.calculation.minutesToSober = (totalMinutesToSober - (data.calculation.hoursToSober * 60)).toInt();
    return bac;
  }

  Future createCalculation({ List<ConsumedDrinkModel> consumedInSession, DateTime start }) async{
    if(consumedInSession == null) consumedInSession = [];
    if(start == null) start = DateTime.now();
    calculation = new CalculationModel(bac: 0, consumedDrinks: consumedInSession, start: start);
  }
  
  Future clearCalculation() async{
    calculation = null;
    if(currentSession != null){
      await endSession();
    }
    
    notifyListeners();    
  }

  /* CALCULATION END */

  /* SESSION START */

  Future setExistingSession() async {
    if(currentSession == null){
      
      final db = await database;
      var sessions = await db
        .query(SessionsTable.TABLE_NAME, columns: [
          SessionsTable.COLUMN_ID, 
          SessionsTable.COLUMN_START, 
          SessionsTable.COLUMN_END, 
          SessionsTable.COLUMN_LAST_DRINK_ADDED_AT, 
          SessionsTable.COLUMN_ALCOHOL_LEVEL, 
          SessionsTable.COLUMN_IS_ACTIVE, 
        ], where: "${ SessionsTable.COLUMN_IS_ACTIVE } = ?", whereArgs: [1]);


      if(sessions.length > 0){
        currentSession = SessionModel.fromMap(sessions.last);
      }
      notifyListeners();
    }
  }

  Future setSession([DateTime now, bool createNew = true]) async {
    if(currentSession == null){
      if(now == null) now = DateTime.now();
      await setExistingSession();
      if(currentSession == null && createNew) {
        await createSession();
      }
      notifyListeners();
    }
  }

  Future createSession({DateTime now, double alcoholLevel = 0,}) async {
    if(now == null) now = DateTime.now();
    final db = await database;
    var newSession = SessionModel(start: now, isActive: true, alcoholLevel: alcoholLevel, lastDrinkAddedAt: now, end: null);
    newSession.id = await db.insert(SessionsTable.TABLE_NAME, newSession.toMap());
    currentSession = newSession;
    notifyListeners();
  }
  
  Future endSession() async{
    if(currentSession != null){
      DateTime now = DateTime.now();
      final db = await database;
      currentSession.end = now;
      currentSession.isActive = false;
      await db.update(
        SessionsTable.TABLE_NAME,
        currentSession.toMap(),
        where: "${ SessionsTable.COLUMN_ID } = ?",
        whereArgs: [currentSession.id]
      );
      currentSession = null;
    }
  }

  Future addSessionDrink(DateTime drinkAddedAt) async {
    if(currentSession != null){
      final db = await database;
      final double currentAlcoholLevel = data.calculation.alcoholLevel;

      currentSession.lastDrinkAddedAt = drinkAddedAt;
      currentSession.alcoholLevel = currentAlcoholLevel;
      await db.update(
        SessionsTable.TABLE_NAME,
        currentSession.toMap(),
        where: "${ SessionsTable.COLUMN_ID } = ?",
        whereArgs: [currentSession.id]
      );
    } 
  }

  /* Future updateSessionAlcoholLevel(double alcoholLevel) async {
    if(currentSession != null){
      final db = await database;
      currentSession.alcoholLevel = alcoholLevel;
      await db.update(
        SessionsTable.TABLE_NAME,
        currentSession.toMap(),
        where: "${ SessionsTable.COLUMN_ID } = ?",
        whereArgs: [currentSession.id]
      );
    } 
  } */

  /* SESSION END */

  /* CONFIG START */

  Future setConfig() async {
    if(config == null){
      final db = await database;

      var configs = await db
          .query(ConfigTable.TABLE_NAME, columns: [
            ConfigTable.COLUMN_ID, 
            ConfigTable.COLUMN_GENDER, 
            ConfigTable.COLUMN_METABOLISM, 
            ConfigTable.COLUMN_WEIGHT, 
            ConfigTable.COLUMN_BAC_DRIVING_LIMIT, 
            ConfigTable.COLUMN_SOUNDS_ENABLED, 
            ConfigTable.COLUMN_CONFIRM_CHOICES
          ]);


      if(configs.length > 0){
        config = ConfigModel.fromMap(configs.first);
      }
      notifyListeners();
    }
  }
  
  Future<ConfigModel> getConfig() async {
    if(config == null){
      final db = await database;

      var configs = await db
          .query(ConfigTable.TABLE_NAME, columns: [
            ConfigTable.COLUMN_ID, 
            ConfigTable.COLUMN_GENDER, 
            ConfigTable.COLUMN_METABOLISM, 
            ConfigTable.COLUMN_WEIGHT, 
            ConfigTable.COLUMN_BAC_DRIVING_LIMIT, 
            ConfigTable.COLUMN_SOUNDS_ENABLED, 
            ConfigTable.COLUMN_CONFIRM_CHOICES
          ]);


      if(configs.length > 0){
        config = ConfigModel.fromMap(configs.first);
      }
    }
    return config;
  }

  Future updateConfig({ int gender, int metabolism, double weight, double bacLimit, bool soundsEnabled, bool confirmChoices }) async {
    if(gender != null){
      config.gender = gender;
    }
    if(metabolism != null){
      config.metabolism = metabolism;
    }
    if(weight != null){
      config.weight = weight;
    }
    if(bacLimit != null){
      config.bacDrivingLimit = bacLimit;
    }
    if(soundsEnabled != null){
      config.soundsEnabled = soundsEnabled;
    }
    if(confirmChoices != null) {
      config.confirmChoices = confirmChoices;
    }
    notifyListeners();
    await updateDatabaseConfig(false);
  }

  Future updateDatabaseConfig(bool notify) async {
    final db = await database;

    await db.update(
      ConfigTable.TABLE_NAME,
      config.toMap(),
    );

    if(notify){
      notifyListeners();
    }
  } 

  /* CONFIG END */

  /* DRINK TYPES START */  

  Future setDrintTypes() async {
    if(drinkTypesList == null){
      final db = await database;

      var drinkTypes = await db
          .query(DrinkTypesTable.TABLE_NAME, columns: [DrinkTypesTable.COLUMN_ID, DrinkTypesTable.COLUMN_NAME, DrinkTypesTable.COLUMN_ALCOHOL, DrinkTypesTable.COLUMN_ALCOHOL_IN_GRAMS, DrinkTypesTable.COLUMN_AMOUNT, DrinkTypesTable.COLUMN_UNITS, DrinkTypesTable.COLUMN_ICON_CODE]);

      drinkTypesList = [];

      drinkTypes.forEach((currentDrinkType) {
        DrinkTypeModel drinkType = DrinkTypeModel.fromMap(currentDrinkType);

        drinkTypesList.add(drinkType);
      });
      notifyListeners();
    }
    return;
  }

  Future<List<DrinkTypeModel>> getDrinkTypes() async {
    if(drinkTypesList == null){
      final db = await database;

      var drinkTypes = await db
          .query(DrinkTypesTable.TABLE_NAME, columns: [DrinkTypesTable.COLUMN_ID, DrinkTypesTable.COLUMN_NAME, DrinkTypesTable.COLUMN_ALCOHOL, DrinkTypesTable.COLUMN_ALCOHOL_IN_GRAMS, DrinkTypesTable.COLUMN_UNITS, DrinkTypesTable.COLUMN_AMOUNT]);

      drinkTypesList = [];

      drinkTypes.forEach((currentDrinkType) {
        DrinkTypeModel drinkType = DrinkTypeModel.fromMap(currentDrinkType);

        drinkTypesList.add(drinkType);
      });
    }
    return drinkTypesList;
  }

  Future<DrinkTypeModel> getDrinkType(int id) async {
    List<DrinkTypeModel> drinkTypes = await getDrinkTypes();
    DrinkTypeModel drinkType = drinkTypes.firstWhere((x) => x.id == id);
    return drinkType;
  }

  Future<DrinkTypeModel> insertDrinkType(DrinkTypeModel drinkType) async {
    final db = await database;
    drinkType.id = await db.insert(DrinkTypesTable.TABLE_NAME, drinkType.toMap());
    if(drinkTypesList != null){
      drinkTypesList.add(drinkType);
    }
    return drinkType;
  }
  
  Future<int> deleteDrinkType(int id) async {
    final db = await database;

    final int count = await db.delete(
      DrinkTypesTable.TABLE_NAME,
      where: "id = ?",
      whereArgs: [id],
    );

    if(count >= 1 && drinkTypesList != null){
      final int deletedIndex = drinkTypesList.indexWhere((x) => x.id == id);
      if(deletedIndex > -1){
        drinkTypesList.removeAt(deletedIndex);
      }
    }

    return count;
  }

  Future<int> updateDrinkType(DrinkTypeModel drinkType) async {
    final db = await database;

    final int count = await db.update(
      DrinkTypesTable.TABLE_NAME,
      drinkType.toMap(),
      where: "id = ?",
      whereArgs: [drinkType.id],
    );

    if(count >= 1 && drinkTypesList != null){
      final int updatedIndex = drinkTypesList.indexWhere((x) => x.id == drinkType.id);
      if(updatedIndex > -1){
        drinkTypesList[updatedIndex] = drinkType;
      }
    }
    return count;
  }

  /* DRINK TYPES END */

  /* CONSUMED DRINKS START */

  Future setConsumedDrinks() async {
    if(consumedDrinksList == null){
      final db = await database;

      var consumedDrinks = await db
          .query(ConsumedDrinksTable.TABLE_NAME, columns: [ConsumedDrinksTable.COLUMN_ID, ConsumedDrinksTable.COLUMN_DRINK_TYPE_ID, ConsumedDrinksTable.COLUMN_SESSION_ID, ConsumedDrinksTable.COLUMN_CONSUMED_AT], orderBy: "${ ConsumedDrinksTable.COLUMN_ID } DESC");

      consumedDrinksList = [];

      consumedDrinks.forEach((currentDrinkType) {
        ConsumedDrinkModel drinkType = ConsumedDrinkModel.fromMap(currentDrinkType);
        consumedDrinksList.add(drinkType);
      });
      notifyListeners();
    }
    return;
  }

  Future<List<ConsumedDrinkModel>> getConsumedDrinksInSession(int sessionId) async {
    if(consumedDrinksList == null){
      await setConsumedDrinks();
    }
    var consumedInSession = consumedDrinksList.where((x) => x.sessionId == sessionId).toList();
    return consumedInSession;
  }

  Future insertConsumedDrink([ConsumedDrinkModel consumedDrink, bool recalculate = false]) async {
    calculationTimer.cancel();
    final db = await database;
    bool createdNewSession = false;
    var drinkType = drinkTypesList.firstWhere((x) => x.id == consumedDrink.drinkTypeId);
    if(currentSession == null){
      await createSession(alcoholLevel: drinkType != null ? drinkType.alcoholInGrams : 0);
      createdNewSession = true;
    }
    consumedDrink.sessionId = currentSession.id;
    consumedDrink.id = await db.insert(ConsumedDrinksTable.TABLE_NAME, consumedDrink.toMap());
    if(consumedDrinksList == null) await setConsumedDrinks();
    consumedDrinksList.insert(0, consumedDrink);
    if(recalculate){
      await calculate(consumedDrink);
      if(!createdNewSession){
        await addSessionDrink(consumedDrink.consumedAt);
      }
    }
    notifyListeners();
    setupCalculationCycle(false);
  }

  /* CONSUMED DRINKS END */

  /* HELPER METHODS START */

  double getCurrentAlcoholLevel(){
    if(currentSession != null && currentSession.lastDrinkAddedAt != null){
      DateTime now = DateTime.now();
      final double burnedPrHour = getAlcoholGramsBurnedPrHour();
      final double hoursSinceLastDrink = now.difference(currentSession.lastDrinkAddedAt).inMinutes / 60;
      final double burned = burnedPrHour * hoursSinceLastDrink;
      double currentAlcoholLevel = currentSession.alcoholLevel - burned;
      if(currentAlcoholLevel < 0){
        currentAlcoholLevel = 0;
      }
      return currentAlcoholLevel;
    }
    return 0;
  }

  double getMinutesToSober(){
    if(calculation != null && calculation.alcoholLevel != 0 && calculation.alcoholLevel > 0) {
      double minutesToSober = (calculation.alcoholLevel / getAlcoholGramsBurnedPrHour()) * 60;
      return minutesToSober;
    }
    return 0;
  }

  double getAlcoholGramsBurnedPrHour() {
    //return ((data.metabolisms[data.config.metabolism].tolerance / 10) * data.config.weight) * 10;
    return ((data.config.weight / 10) * data.metabolisms[data.config.metabolism].tolerance);
  }

  /* HELPER METHODS END */
  
}
