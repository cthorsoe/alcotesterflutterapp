class DoubleFormatter {
  static RegExp _formatter = RegExp(r"([.]*0)(?!.*\d)");
  
  static String format(double value) {
    return value.toString().replaceAll(_formatter, "");
  }
}