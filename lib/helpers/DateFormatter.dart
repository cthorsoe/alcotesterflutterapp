import 'package:intl/intl.dart';

class DateFormatter {

  static final DateFormat _dateFormat = DateFormat("dd-MM-yyyy HH:mm");
  static final DateFormat _todayFormat = DateFormat("'Today at' HH:mm");
  static final DateFormat _yesterdayFormat = DateFormat("'Yesterday at' HH:mm");
  
  static String formatDate([DateTime date, bool formatAsToday = false, bool formatAsYesterday = false, bool lowerCase = false]) {
    String formattedDate;
    if(formatAsToday && _isToday(date)){
      formattedDate = _todayFormat.format(date);
    } else if(formatAsYesterday && _isYesterday(date)){
      formattedDate = _yesterdayFormat.format(date);
    }else {
      formattedDate = _dateFormat.format(date);
    }
    return lowerCase ? formattedDate.toLowerCase() : formattedDate;
  }

  static bool _isToday(DateTime date) {
    DateTime now = DateTime.now();
    return date.day == now.day && date.month == now.month && date.year == now.year;
  }

  static bool _isYesterday(DateTime date) {
    DateTime now = DateTime.now();
    return (date.day + 1) == now.day && date.month == now.month && date.year == now.year;
  }
}