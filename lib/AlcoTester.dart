import 'package:alcotester_flutter_app/variables/AppVariables.dart';
import 'package:flutter/material.dart';
import 'package:alcotester_flutter_app/models/ViewModel.dart';
import 'package:alcotester_flutter_app/views/BACCalculator.dart';
import 'package:alcotester_flutter_app/views/Consumed.dart';
import 'package:alcotester_flutter_app/views/Information.dart';
import 'package:alcotester_flutter_app/views/Settings.dart';
import 'package:alcotester_flutter_app/db/DataProvider.dart';

class AlcoTester extends StatefulWidget {
  AlcoTester({Key key}) : super(key: key);
  

  @override
  AlcoTesterState createState() => AlcoTesterState();
}

class AlcoTesterState extends State<AlcoTester> {
  int _selectedIndex = 0;

  List<String> _viewTitles = <String>[
    "BAC Calculator",
    "Consumed List",
    "Settings"
  ];

  String getViewTitle(){
    return _viewTitles[_selectedIndex];
  }

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }  

  void openInfo() {
    Navigator.of(context)
      .push(MaterialPageRoute(builder: (BuildContext context) {
        return Information();
      }
    ));
  }

  @override
  void initState() {
    super.initState();
    DataProvider.data.setConfig();
    DataProvider.data.setExistingSession();
    DataProvider.data.setConsumedDrinks();
    DataProvider.data.setDrintTypes();
    DataProvider.data.setupCalculationCycle();
  }

  @override
  Widget build(BuildContext context) {
    print('alcotester build');
    List<ViewModel> _widgetOptions = <ViewModel>[
      new ViewModel("BAC Calculator", BACCalculator()),
      new ViewModel("Consumed", Consumed()),
      new ViewModel("Settings", Settings())
    ];
    return Scaffold(
      appBar: AppBar(
        title: Text(
          _widgetOptions.elementAt(_selectedIndex).title, 
          style: TextStyle(
            color: Colors.white,
            fontSize: AppVariables.AppBarFontSize
          )
        ),
        centerTitle: true,
        iconTheme: IconThemeData(
          color: Colors.white,
        ),
        actions: <Widget> [
          IconButton(icon: Icon(Icons.info_outline), onPressed: () => { openInfo() })
        ],
      ),
      body: Container(
        child: _widgetOptions.elementAt(_selectedIndex).widget,
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem> [
          BottomNavigationBarItem(
            icon: Icon(Icons.local_bar),
            label: 'Calculator',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.list),
            label: 'Consumed',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.settings),
            label: 'Settings'
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: Colors.blue[400],
        selectedFontSize: 12,
        selectedLabelStyle: TextStyle(fontWeight: FontWeight.bold),
        onTap: _onItemTapped)
    );
  }
}