class AppVariables {
  static const double AppFontSize = 16;
  static const double AppRegularHeaderFontSize = 18;
  static const double AppBarFontSize = 22;

  static const double CalculatorBACFontSize = 84;

  static const double ConsumedListViewPadding = 8;
  static const double ConsumedListItemHeaderFontSize = 18;
  static const double ConsumedListItemVerticalPadding = 16;

  static const double SettingsListItemFontSize = 16;
  static const double SettingsListItemHorizontalPadding = 16;
  static const double SettingsListItemVerticalPadding = 4;
  static const double SettingsDropDownIconSize = 24;
  static const int SettingsDropDownElevation = 16;

  static const double InformationAlertFontSize = 18;
  static const double InformationParagraphVericalPadding = 4;
  static const double InformationDrinkTypeHeaderFontSize = 18;
  static const double InformationDrinkTypeContentFontSize = 14;
  static const double InformationHeaderFontSize = 34;

}
